#ifndef GNUPLOTBASE_HH_
#define GNUPLOTBASE_HH_

#include <stdio.h>

class GnuplotPipe
{
    friend class GnuplotBase;

    private:
        //The gnuplot program
        FILE* gnuplotpipe;
    protected:
        //Constructor and Destructor
        GnuplotPipe() { gnuplotpipe=popen(" gnuplot","w"); }
        ~GnuplotPipe() { pclose(gnuplotpipe); }
};

class GnuplotBase : GnuplotPipe
{
    friend class Gnuplot;

    private:
        //Pipes output to gnuplot
        GnuplotBase& operator <<(const char*  command) { fprintf(gnuplotpipe, "%s", command); return *this; }
        GnuplotBase& operator <<(const double command) { fprintf(gnuplotpipe, "%f", command); return *this; }

        //Flushes the buffer
        void operator <(const char*  command) { fprintf(gnuplotpipe, "%s", command); fflush(gnuplotpipe); }

    protected:
        GnuplotBase() : GnuplotPipe() {}
};

#endif
