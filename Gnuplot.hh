#ifndef GNUPLOT_HH_
#define GNUPLOT_HH_

#include "GnuplotBase.hh"

class Gnuplot : GnuplotBase
{
    public:
        //Constructor
        Gnuplot() : GnuplotBase() {}
        
        //Clear the screen
        void clear();

        //Use gnuplot "set" command
        void set(const char* option);

        //Use gnuplot "plot" command
        void plot(double* x, int x_N, int x_step, double* y);
        void plot(double* x, int x_N,             double* y) { plot(x, x_N, 1, y); }

        //Use gnuplot "splot" command
        void splot(double* x, int x_N, int x_step, double* y, int y_N, int y_step, double** z);
        void splot(double* x, int x_N,             double* y, int y_N,             double** z) { splot(x, x_N, 1, y, y_N, 1, z); }
};

#endif
